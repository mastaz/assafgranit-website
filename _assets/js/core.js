(function () {
    "use strict";

    function init_menu() {
      $(".hamburger").click(function() {
        $(".header").toggleClass("--open");

        if ($(".header").is(".--open")) {
          $("body").css("overflow", "hidden");
        } else {
          $("body").css("overflow", "auto");
        }
      });

      $(".menu").find("a").click(function() {
        if ($(".header").is(".--open")) {
          $(".header").removeClass("--open");
          $("body").css("overflow", "auto");
        }
      });

      $(window).scroll(function(){
        if ($(window).scrollTop() > 0) {
          $('.header').addClass('--scrolled-away');
        }
        else {
          $('.header').removeClass('--scrolled-away');
        }
      });
    }

    function init_lazy() {
      $('.lazy').Lazy({
        delay: -1,
        combined: true,
      });

      // Remove the placeholder image from the cover images
      $('.lazy-background').css("background-image", "");
    }

    function init_scroll_animation() {
      $('a[href*="#"]:not([href="#"]):not(".--no-scrolling")').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    }

    function init_masonry() {
      $('.grid').isotope({
        layoutMode: 'packery',
        packery: {
          gutter: '.gutter',
          // percentPosition: true,
        },
        itemSelector: '.item',
      });
    }

    function init_carousel() {
      $(".carousel").slick();
    }

    function init_clamping() {
      $(".clamp").each(function(index, element) {
        var config;

        if (this.dataset.clamp) {
          config = JSON.parse(this.dataset.clamp);
          config.useNativeClamp = false;
        }

        $(element).data("original", $(element).html());

        $clamp(element, config);

        $(element).toggleClass("--clamped");
      });

      $(".clamp-toggle").click(function(e) {
        var that = this;

        // e.preventDefault();

        $(that.dataset.toggle).each(function(index, element) {
          var config = JSON.parse(element.dataset.clamp);
          config.useNativeClamp = false;

          $(element).toggleClass("--clamped");

          if (!$(element).hasClass("--clamped")) {
            config.clamp = "auto";
            $(element).html($(element).data("original"));
            $(that).text(that.dataset.less);
          } else {
            $(that).text(that.dataset.more);
          }

          $clamp(element, config);
        });

        $(window).trigger('resize.px.parallax');
      });
    }

    function init_popup() {
      $('.open-popup')
        .on('click', function(e) {
          e.preventDefault();

          var item = $(this.dataset.item);
          var target = $(this).attr("href");
          var title = $(item).find(".title").find("h3").text();
          var description = $(item).find(".description").find("p").text();
          $(target).find(".popup__teaser").find(".title").find("h3").text(title);
          $(target).find(".popup__teaser").find(".description").find("p").text(description);
          $(target).find(".selected-item").val(title);
          $(target).find(".selected-item").val(description);

          $(this)
            .magnificPopup({
              removalDelay: 300,
              mainClass: 'mfp-fade  mfp-popup',
              type: 'inline',
              preloader: false,
              focus: '#name',
              fixedContentPos: false,
              callbacks: {
                beforeOpen: function() {
                  $("body").css("overflow", "hidden");

                  if($(window).width() < 700) {
                    this.st.focus = false;
                  } else {
                    this.st.focus = '#name';
                  }
                },
                beforeClose: function() {
                  $("body").css("overflow", "");
                },
              }
            })
            .magnificPopup('open');
        });

      $('.open-youtube-popup').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
        callbacks: {
          beforeOpen: function() {
            $("body").css("overflow", "hidden");
          },
          beforeClose: function() {
            $("body").css("overflow", "");
          },
        }
      });
    }

    function init_load_more() {
      var THRESHOLD = 3;

      $(".load-more").click(function(e) {
        e.preventDefault();

        $(".lectures-and-workshops")
          .find(".item.--not-loaded")
          .slice(0, THRESHOLD)
          .each(function(index, item) {
            $(item).removeClass("--not-loaded");
          });

        if ($(".lectures-and-workshops").find(".item.--not-loaded").length === 0) {
          $(this).hide();
        }
      });
    }

    function init_lightslider() {
      $('.--gallery.--restaurants').each(function() {
        var isRTL = $("html").attr("lang") === 'he-IL';
        var lightSlider = $(this).find('.lightSlider').lightSlider({
          gallery: true,
          item: 1,
          loop: true,
          slideMargin: 0,
          thumbItem: 8,
          controls: false,
          rtl: isRTL,
        });

        $(this).find('.gallery-prev')
          .on('click', function(e) {
            e.preventDefault();
            lightSlider.goToPrevSlide()
          });

        $(this).find('.gallery-next')
          .on('click', function(e) {
            e.preventDefault();
            lightSlider.goToNextSlide()
          });
      });

      $('.open-gallery')
        .on('click', function() {
          $(this)
            .magnificPopup({
              type: 'inline',
              mainClass: 'mfp-fade  mfp-gallery',
              removalDelay: 160,
              preloader: false,
              fixedContentPos: false,
              callbacks: {
                beforeOpen: function() {
                  $("body").css("overflow", "hidden");
                },
                beforeClose: function() {
                  $("body").css("overflow", "");
                },
              }
            })
            .magnificPopup('open');
        });

      setTimeout(function() {
        $(".lightSlider")
          .closest(".popup")
          .addClass("mfp-hide")
          .css("opacity", 1);
      }, 1000);
    }

    function init_animations() {
      $('.animated').waypoint(function() {
        var $element = $($(this)[0].element);
        var animation = $element.attr('data-animation');
        $element.addClass(animation);
      }, {
        offset: '85%'
      });

      $('.animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
          $(this).removeClass('animated');
      });
    }

    function init_contact() {
      $(".contact").each(function() {
        var $form = $(this);

        $form.validate({
          rules: {
            "name": "required",
            "phone": {
              required: true
            },
            "email": {
              required: true,
              email: true
            },
            "message": {
              required: true,
              minlength: 5
            }
          },
          messages: {
            "name": "",
            "phone": "",
            "email": "",
            "message": ""
          }
        });

        $form.submit(function (e) {
          var $form;
          var $result;

          $form = $(this);

          // Disable the postback
          e.preventDefault();

          // Validate form
          $result = $form.validate();

          if ($result.errorList.length === 0) {
            $.ajax({
              type: $form.attr("method"),
              url: $form.attr("action"),
              data: $form.serialize(),
              success: function (data) {
                if (data.status == "success") {
                  $form.addClass("--submitted");

                  // Clear the form
                  $("input, textarea").val("");
                }
              },
              error: function (event, jqxhr, settings, thrownError) {
                $form.addClass("--error");
              }
            });
          }
        });
      });
    }

    function init_parallax() {
      if($(window).width() > 1040) {
        $.stellar({
          horizontalScrolling: false,
          responsive: true
        });
      }
    }

    function init() {
      init_menu();
      init_lazy();
      init_scroll_animation();
      init_masonry();
      init_carousel();
      init_clamping();
      init_popup();
      init_load_more();
      init_lightslider();
      init_animations();
      init_contact();
      init_parallax();
    }

    init();
})();
